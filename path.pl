
#-------------------------------------------------------------------------------------------
#------Checking the existence of the directories included in PATH environment variable------
#-------------------------------------------------------------------------------------------

@path_dir = split(':',$ENV{PATH});

%myhash=();


foreach $dir (@path_dir)
{

if(-e $dir)
{

if(-x $dir)
{

$myhash{'Existing directories with executable permissions'}{$dir}=$dir;


}
else
{

$myhash{'Existing directories with no executable permissions'}{$dir}=$dir;

}


}
else
{


$myhash{'Non-Existing directories'}{$dir}=$dir;

}

}


print "----------Please find the result of each location in the PATH variable below ----------- \n\n";


print "----------List of Existing directories with executable permissions ------------ \n\n";

print_result('Existing directories with executable permissions');

print "\n------------------------------------------------------------------------------------\n\n";


print "----------List of Existing directories with no executable permissions ------------ \n\n";

print_result('Existing directories with no executable permissions');

print "\n------------------------------------------------------------------------------------\n\n";

print "----------List of Non-Existing directories ------------ \n\n";

print_result('Non-Existing directories');

print "\n------------------------------------------------------------------------------------\n\n";


#--------------------------------------------------------------------------------------------
#-----Subroutine to print the result based on the categories mentioned-----------------------
#--------------------------------------------------------------------------------------------


sub print_result
{

my ($category)=@_;

foreach $mykey (keys %{$myhash{$category}})
{

print "$myhash{$category}{$mykey}";

print "\n";

}

}


