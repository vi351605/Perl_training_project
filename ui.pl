
#---------------------------------------------------------------------------------------------------------
#Perl Programming for getting standard i/p through stdin and storing the user given inputs into a file
#---------------------------------------------------------------------------------------------------------


@question=("Name of the file : ","First name : ","Last name : ","e-mail address : ");

%answer=();


#--------------------------------------------------------------------------------------------------------- 
#Iteration for storing the user inputs in a hash 
#---------------------------------------------------------------------------------------------------------


foreach $ques (@question)
{

print "$ques";

$answer{$ques}=<STDIN>;

}


#--------------------------------------------------------------------------------------------------------- 
#Opening a file with name "myuserfile" and writing the given inputs stored in hash into a file 
#---------------------------------------------------------------------------------------------------------


open(MYFILE,">myuserfile");

foreach $mykey (keys %answer)
{

print MYFILE "$mykey$answer{$mykey}"; 


}

close(MYFILE);



